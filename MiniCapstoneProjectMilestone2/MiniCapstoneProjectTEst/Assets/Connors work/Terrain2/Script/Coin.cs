using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int coinValue;

    void OnTriggerEnter(Collider other)
    {
        PlayerData data = other.GetComponent<PlayerData>();
        if (data!=null)
        {
            Destroy(gameObject);
            LevelManager.Instance.KeepScore(coinValue);
        }
    }
}
