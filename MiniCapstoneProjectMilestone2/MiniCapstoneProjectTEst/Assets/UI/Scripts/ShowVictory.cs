using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowVictory : MonoBehaviour
{
    public void OnVictory()
    {
        gameObject.SetActive(true);
    }
}
