using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class PlayerSpawningState : PlayerBaseState
{
    private bool spawned;
    private float timer = 0.0f;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        spawned = false;
        characterAnimator.SetBool("Spawn", true);
        playableDirector.stopped += OnPlayerSpawned;
        playerData.RefillHealth();
        playableDirector.Play();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer += Time.deltaTime;

        if(timer == 5)
        {
            spawned = true;
        }
        
        //Finish Camera work then transition to controlling state
        if (spawned)
        {
            fsm.ChangeState(fsm.ControllingStateName);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        characterAnimator.SetBool("Spawn", false);
        playableDirector.stopped -= OnPlayerSpawned;
    }

    public void OnPlayerSpawned(PlayableDirector aDirector)
    {
        spawned = true;
    }
}
