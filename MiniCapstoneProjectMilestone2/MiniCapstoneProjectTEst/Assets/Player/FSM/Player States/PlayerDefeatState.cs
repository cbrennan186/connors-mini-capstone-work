using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDefeatState : PlayerBaseState
{
    
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        LevelManager.Instance.Spawn.AddListener(OnDeathSceneEnd);
        characterAnimator.SetTrigger("Killed");
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    { 
        
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        LevelManager.Instance.Spawn.RemoveListener(OnDeathSceneEnd);
    }

    public void OnDeathSceneEnd()
    {
        fsm.ChangeState(fsm.SpawningStateName);
        
    }
}
