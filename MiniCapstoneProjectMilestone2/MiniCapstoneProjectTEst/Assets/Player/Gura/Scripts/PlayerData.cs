using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerData : MonoBehaviour
{
    public float maxHealth = 3;
    private float currentHealth;
    public UnityEvent<float> UpdateHealth;
    public UnityEvent PlayerDefeat;
    public bool isDead;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(float damage)
    {
        if (!isDead)
        {
            if (damage < currentHealth)
            {
                currentHealth -= damage;
            }
            else
            {
                currentHealth = 0;
                isDead = true;
                PlayerDefeat.Invoke();
            }
            UpdateHealth.Invoke(currentHealth);
        }
    }

    public void RefillHealth()
    {
        currentHealth = maxHealth;
        isDead = false;
        UpdateHealth.Invoke(currentHealth);
    }
}
