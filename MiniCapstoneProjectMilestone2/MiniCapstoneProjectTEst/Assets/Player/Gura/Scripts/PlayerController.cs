using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class PlayerController : Singleton<PlayerController>
{
    public InputHandler inputHandler;
    public CameraControl cameraControl;
    public CharacterLocomotion characterLocomotion;
    public Animator characterAnimation;
    public PlayableDirector playableDirector;
    public PlayerData playerData;
}
