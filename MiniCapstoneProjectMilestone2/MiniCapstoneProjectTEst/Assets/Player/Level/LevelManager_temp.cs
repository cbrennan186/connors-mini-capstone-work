using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelManager_temp: Singleton<LevelManager>
{
    public GameObject spawnPoint;
    public GameObject winTrigger;
    public UnityEvent Spawn;

    public float respawnDelay = 5f;
    public float respawnTimer;
    private bool respawnCountDown;
    // Start is called before the first frame update
    void Start()
    {
        respawnTimer = 0;
        PlayerController.Instance.playerData.PlayerDefeat.AddListener(OnPlayerDefeat);
    }

    // Update is called once per frame
    void Update()
    {
        if (respawnCountDown)
        {
            respawnTimer -= Time.deltaTime;
        }
        
        if (respawnTimer <= 0)
        {
            Spawn.Invoke();
            PlayerController.Instance.characterLocomotion.transform.position = spawnPoint.transform.position;
            PlayerController.Instance.characterLocomotion.transform.rotation = spawnPoint.transform.rotation;
            respawnTimer = respawnDelay;
            respawnCountDown = false;
        }
    }

    public void OnPlayerDefeat()
    {
        respawnCountDown = true;
    }
}
