using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Rigidbody g_Rigidbody;
    float g_Speed;

    // Start is called before the first frame update
    void Start()
    {
        g_Rigidbody = GetComponent<Rigidbody>();
        g_Speed = 10.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            g_Rigidbody.velocity = transform.up * g_Speed;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            g_Rigidbody.velocity = -transform.up * g_Speed;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            g_Rigidbody.velocity = -transform.forward * g_Speed;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            g_Rigidbody.velocity = transform.forward * g_Speed;
        }


        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            g_Rigidbody.velocity = new Vector2(0, 0);
        }

        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            g_Rigidbody.velocity = new Vector2(0, 0);
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            g_Rigidbody.velocity = new Vector2(0, 0);
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            g_Rigidbody.velocity = new Vector2(0, 0);
        }
    }
}
