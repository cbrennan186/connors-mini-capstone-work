using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;


public class PlayerControllerNetwork : MonoBehaviourPun
{

    Rigidbody g_Rigidbody;
    float g_Speed;

    AudioSource jumpSound;
    AudioSource collisionSound;

    //public Text _PlayerNickName;


    // Start is called before the first frame update
    void Start()
    {
       
       // if (photonView.IsMine == true)
       // {
       //     _PlayerNickName = GameObject.Find("PlayerNickName").GetComponent<Text>();
       //     _PlayerNickName.text = photonView.Owner.NickName;

           // photonView.RPC("UpdateNickName", RpcTarget.All, _PlayerNickName.text);
       // }

        g_Rigidbody = GetComponent<Rigidbody>();
        //g_Rigidbody.isKinematic = true;
       // g_Speed = 10.0f;
        g_Speed = 30.0f;

        jumpSound = GetComponent<AudioSource>();     
    }

    //[PunRPC]
   // public void UpdateNickName()
    //{
   //     _PlayerNickName.text = photonView.Owner.NickName;
  //  }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine == false)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            g_Rigidbody.velocity = transform.up * g_Speed;
            jumpSound.Play();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            g_Rigidbody.velocity = -transform.up * g_Speed;
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            g_Rigidbody.velocity = -transform.forward * g_Speed;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            g_Rigidbody.velocity = transform.forward * g_Speed;
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            g_Rigidbody.transform.Rotate(0.0f, 10.0f, 0.0f, Space.World);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            g_Rigidbody.transform.Rotate(0.0f, -10.0f, 0.0f, Space.World);
        }


        if (Input.GetKeyUp(KeyCode.W))
        {
            g_Rigidbody.velocity = new Vector2(0, 0);
        }

        if (Input.GetKeyUp(KeyCode.S))
        {
            g_Rigidbody.velocity = new Vector2(0, 0);
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            g_Rigidbody.velocity = new Vector2(0, 0);
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            g_Rigidbody.velocity = new Vector2(0, 0);
        }
    }
}
